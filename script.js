// draw div function
var drawDivs = function(numBoxes)
{
	console.log(numBoxes);
	for(var x= 0 ; x < numBoxes; ++x)
	{
		//make div to wrap a row of divs
		$("#container").append('<div class="row" ></div>');
		//console.log("outer loop")
	}

	for(var y=0 ; y < numBoxes; ++y)
	{
		//console.log('inner loop');
		$(".row").append('<div class=innerBox></div>');
	}

	var borderwidth = 1;
	var dimension = Math.floor(((900-borderwidth*2*numBoxes )/(numBoxes)));
	$('.row').height(dimension +2*borderwidth);
	$('.row').width(numBoxes*dimension + (2*borderwidth*numBoxes));
	$('.innerBox').width(dimension);
	$('.innerBox').height(dimension);
};

var mouseEnterEffects = function()
{
	//set this div to grey
	$(this).css('background-color','grey');
};

var mouseLeaveEffect = function()
{
	$(this).css('background-color', 'black');
};

var clearButton = function()
{
	$('#container').empty();

	var size = prompt('what grid size do you want?');

	drawDivs(size);

	$('.innerBox').hover(mouseEnterEffects,mouseLeaveEffect);
};

$(document).ready
(function() {
	// make divs
	drawDivs(16);
	$('.innerBox').hover(mouseEnterEffects,mouseLeaveEffect);
});
